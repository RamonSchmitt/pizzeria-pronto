export interface Order {
	order: Item[];
	address: Address;
}

export interface Address {
	name: string;
	street: string;
	houseNumber: number;
	houseNumberExt?: string;
	zipcode: string;
	city: string;
	phoneNumber: string;
}

export interface Item {
	crust: string;
	size: string;
	ingredients: number[];
}
