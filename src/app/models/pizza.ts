export interface Pizza {
	'id': number;
	'title': string;
	'ingredients': [];
}

export interface Ingredient {
	'id': number;
	'title': string;
	'stock': number;
}
