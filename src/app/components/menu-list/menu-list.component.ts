import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { Pizza } from '../../models';
import { MenuService } from '../../services';
import { MenuActions } from '../../actions';

@Component({
	selector: 'app-menu-list',
	templateUrl: './menu-list.component.html',
	styleUrls: ['./menu-list.component.scss'],
})
export class MenuListComponent implements OnInit {
	@select(state => state.menu.menuList)
	readonly menuList$: Observable<Pizza[]>;

	menuList: Pizza[];

	constructor(
		private menuService: MenuService,
		private menuActions: MenuActions
	) {}

	ngOnInit() {
		this.menuService.getMenuList();
		this.menuList$.subscribe((menuList: Pizza[]) => {
			this.menuList = menuList;
		});
	}

	selectPizza(pizza) {
		this.menuActions.selectPizza(pizza);
	}
}
