import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/testing';

import { MenuListComponent } from './menu-list.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MenuActions } from 'src/app/actions/menu.actions';

describe('MenuListComponent', () => {
	let component: MenuListComponent;
	let fixture: ComponentFixture<MenuListComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [MenuListComponent],
			imports: [NgReduxTestingModule],
			providers: [
				HttpClient,
				HttpHandler,
				MenuActions,
			]
		}).compileComponents();

		MockNgRedux.reset();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MenuListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		const stub = MockNgRedux.getSelectorStub(['menu', 'menuList']);
		// const stub = MockNgRedux.getSelectorStub(['elephant', 'loading']);
		// stub.next(false);
		// stub.next(true);
		// stub.complete();
		//
		// elephantPage.loading$
		// 	.toArray()
		// 	.subscribe(
		// 		actualSequence => expect(actualSequence).toEqual([false, true]),
		// 		null,
		// 		done);
		expect(component).toBeTruthy();
	});
});
