import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressFormComponent } from './address-form.component';
import { FormsModule } from '@angular/forms';
import { OrderActions } from 'src/app/actions/order.actions';
import { NgRedux } from '@angular-redux/store';
import { HttpHandler, HttpClient } from '@angular/common/http';

describe('AddressFormComponent', () => {
	let component: AddressFormComponent;
	let fixture: ComponentFixture<AddressFormComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
			],
			declarations: [AddressFormComponent],
			providers: [
				HttpClient,
				HttpHandler,
				OrderActions,
				NgRedux
			]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddressFormComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
