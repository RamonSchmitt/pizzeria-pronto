import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OrderActions } from 'src/app/actions/order.actions';
import { OrderService } from 'src/app/services/order.service';

@Component({
	selector: 'app-address-form',
	templateUrl: './address-form.component.html',
	styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent {
	@ViewChild('orderForm') orderForm: NgForm;
	statusModal = false;
	orderStatus;

	constructor(
		private orderActions: OrderActions,
		private orderService: OrderService) {
	}

	onSubmit() {
		this.orderActions.addAddress(this.orderForm.value.userData);
		this.orderService.placeOrder().subscribe(orderStatus => {
			this.orderStatus = orderStatus;
			this.statusModal = true;
		});
	}

	closeStatusModal() {
		this.statusModal = false;
	}
}
