import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderComponent } from './order.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { FormsModule } from '@angular/forms';
import { OrderActions } from 'src/app/actions/order.actions';
import { NgRedux } from '@angular-redux/store';
import { HttpHandler, HttpClient } from '@angular/common/http';

describe('OrderComponent', () => {
	let component: OrderComponent;
	let fixture: ComponentFixture<OrderComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
			],
			declarations: [
				OrderComponent,
				AddressFormComponent
			],
			providers: [
				HttpClient,
				HttpHandler,
				OrderActions,
				NgRedux
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(OrderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
