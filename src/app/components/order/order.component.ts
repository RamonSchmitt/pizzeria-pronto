import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { Order, Item } from '../../models';
import { Ingredient } from 'src/app/models/pizza';

@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
	styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
	@select(state => state.order)
	readonly order$: Observable<Order>;

	@select(state => state.ingredients)
	readonly ingredients$: Observable<Ingredient[]>;

	itemList: Item[];
	ingredients;

	ngOnInit() {
		this.order$.subscribe(itemList => {
			this.itemList = itemList.order;
		});

		this.ingredients$.subscribe(ingredients => {
			this.ingredients = ingredients;
		});
	}

	usedIngredient(id) {
		return this.ingredients.find(ingredient => ingredient.id === id);
	}
}
