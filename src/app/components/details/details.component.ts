import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { Ingredient, Pizza, Item } from '../../models';
import { OrderActions } from '../../actions';
import { IngredientsService } from '../../services';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
	@select(state => state.menu.selectedPizza)
	readonly selectedPizza$: Observable<Pizza>;

	@select(state => state.ingredients)
	readonly ingredients$: Observable<Ingredient[]>;

	title = 'Pizza Custom';

	item: Item = {
		crust: 'regular',
		size: '14cm',
		ingredients: [],
	};

	usedIngredients: Ingredient[];

	unUsedIngredients: Ingredient[];

	stock = [];

	constructor(
		private orderActions: OrderActions,
		private ingredientsService: IngredientsService
	) {}

	ngOnInit() {
		this.setStock();
		this.setSelectedPizza();
	}

	setStock() {
		this.ingredientsService.getStock();

		this.ingredients$.subscribe((ingredients: Ingredient[]) => {
			this.stock = ingredients;
			this.updateIngredients();
		});
	}

	setSelectedPizza() {
		this.selectedPizza$.subscribe((pizza: Pizza) => {
			if (pizza) {
				this.title = pizza.title;
				this.item.ingredients = pizza.ingredients;
			}
		});
	}

	addItem() {
		this.orderActions.addToOrder(this.item);
		this.item = {
			crust: 'regular',
			size: '14cm',
			ingredients: [],
		};
	}

	selectCrust(selectedCrust) {
		this.item.crust = selectedCrust;
	}

	selectSize(selectedSize) {
		this.item.size = selectedSize;
	}

	addIngredient(title: string) {
		const selectedIngredient = this.stock.find(ingredient => ingredient.title === title);
		this.item.ingredients = [...this.item.ingredients, ...[selectedIngredient.id]];
		this.title = 'Pizza Custom';
		this.updateIngredients();
	}

	removeIngredient(id) {
		this.item.ingredients = this.item.ingredients.filter(ingredient => ingredient !== id);
		this.title = 'Pizza Custom';
		this.updateIngredients();
	}

	private updateIngredients() {
		this.usedIngredients = this.stock.filter(ingredient => this.item.ingredients.includes(ingredient.id));
		this.unUsedIngredients = this.stock.filter(ingredient => !this.item.ingredients.includes(ingredient.id));
	}
}
