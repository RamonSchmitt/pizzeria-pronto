import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsComponent } from './details.component';
import { OrderActions } from 'src/app/actions/order.actions';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { IngredientsActions } from 'src/app/actions/ingredients.actions';
import { IngredientsService } from 'src/app/services/ingredients.service';

describe('DetailsComponent', () => {
	let component: DetailsComponent;
	let fixture: ComponentFixture<DetailsComponent>;

	beforeEach(async(() => {
		const mockIngredientsService = {
			getStock: () => {}
		};

		TestBed.configureTestingModule({
			declarations: [DetailsComponent],
			providers: [
				OrderActions,
				HttpClient,
				HttpHandler,
				IngredientsActions,
				{ provide: IngredientsService, useValue: mockIngredientsService }
			]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
