import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { DetailsComponent } from './components/details/details.component';
import { OrderComponent } from './components/order/order.component';
import { AddressFormComponent } from './components/order/address-form/address-form.component';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				FormsModule,
			],
			declarations: [
				AppComponent,
				MenuListComponent,
				DetailsComponent,
				OrderComponent,
				AddressFormComponent,
			],
		}).compileComponents();
	}));

	it('should create the app', () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	});
});
