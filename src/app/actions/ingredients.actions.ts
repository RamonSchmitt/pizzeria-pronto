import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { Ingredient } from '../models';

@Injectable()
export class IngredientsActions {
	static readonly SET_INGREDIENTS = 'SET_INGREDIENTS';

	@dispatch()
	setIngredients(payload: Ingredient[]) {
		return {
			type: IngredientsActions.SET_INGREDIENTS,
			payload
		};
	}
}
