import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { Item, Address } from '../models';

@Injectable()
export class OrderActions {
	static readonly ADD_TO_ORDER = 'ADD_TO_ORDER';
	static readonly ADD_ADDRESS = 'ADD_ADDRESS';

	@dispatch()
	addToOrder(payload: Item) {
		return {
			type: OrderActions.ADD_TO_ORDER,
			payload
		};
	}

	@dispatch()
	addAddress(payload: Address) {
		return {
			type: OrderActions.ADD_ADDRESS,
			payload
		};
	}
}
