import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { Pizza } from '../models';

@Injectable()
export class MenuActions {
	static readonly SET_MENULIST = 'SET_MENULIST';
	static readonly SELECT_PIZZA = 'SELECT_PIZZA';

	@dispatch()
	setMenu(payload: Pizza[]) {
		return {
			type: MenuActions.SET_MENULIST,
			payload
		};
	}

	@dispatch()
	selectPizza(payload: Pizza) {
		return {
			type: MenuActions.SELECT_PIZZA,
			payload
		};
	}
}
