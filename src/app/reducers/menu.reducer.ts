import { MenuActions } from '../actions';
const INITIAL_STATE = {};

export function menu(state = INITIAL_STATE, action) {
	switch (action.type) {
		case MenuActions.SET_MENULIST:
			return {...state, ...{ menuList: action.payload }};
		case MenuActions.SELECT_PIZZA:
			return {...state, selectedPizza: action.payload};
		default:
			return state;
	}
}
