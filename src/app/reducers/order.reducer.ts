import { OrderActions } from '../actions';

const INITIAL_STATE = {
	order: [],
	address: {
		name: '',
		street: '',
		houseNumber: 0,
		houseNumberExt: '',
		zipcode: '',
		city: '',
		phoneNumber: '',
	}
};

export function order(state = INITIAL_STATE, action) {
	switch (action.type) {
		case OrderActions.ADD_TO_ORDER:
			return {...state, order: [...state.order, action.payload]};
		case OrderActions.ADD_ADDRESS:
			return {...state, address: action.payload};
		default:
			return state;
	}
}
