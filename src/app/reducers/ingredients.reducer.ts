import { IngredientsActions } from '../actions';

const INITIAL_STATE = [];

export function ingredients(state = INITIAL_STATE, action) {
	switch (action.type) {
		case IngredientsActions.SET_INGREDIENTS:
			return action.payload;
		default:
			return state;
	}
}
