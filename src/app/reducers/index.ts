import { combineReducers } from 'redux';
import { menu } from './menu.reducer';
import { order } from './order.reducer';
import { ingredients } from './ingredients.reducer';

export const rootReducer: any = combineReducers({
	menu,
	order,
	ingredients,
});
