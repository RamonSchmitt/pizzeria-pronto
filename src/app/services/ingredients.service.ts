import { Injectable } from '@angular/core';
import { Ingredient } from '../models';
import { HttpClient } from '@angular/common/http';
import { IngredientsActions } from '../actions';

@Injectable({
	providedIn: 'root'
})
export class IngredientsService {
	constructor(
		private http: HttpClient,
		private ingredientsActions: IngredientsActions,
	) {}

	getStock() {
		this.http.get(`http://localhost:3005/ingredients`).subscribe((ingredients: Ingredient[]) => {
			this.ingredientsActions.setIngredients(ingredients);
		});
	}
}
