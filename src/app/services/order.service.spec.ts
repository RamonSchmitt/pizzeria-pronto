import { TestBed } from '@angular/core/testing';

import { OrderService } from './order.service';
import { NgRedux } from '@angular-redux/store';
import { HttpHandler, HttpClient } from '@angular/common/http';

describe('OrderService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		providers: [
			HttpClient,
			HttpHandler,
			NgRedux
		]
	}));

	it('should be created', () => {
		const service: OrderService = TestBed.get(OrderService);
		expect(service).toBeTruthy();
	});
});
