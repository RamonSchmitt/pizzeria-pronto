import { TestBed } from '@angular/core/testing';

import { MenuService } from './menu.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { MenuActions } from '../actions';

describe('MenuService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		providers: [
			HttpClient,
			HttpHandler,
			MenuActions,
		]
	}));

	it('should be created', () => {
		const service: MenuService = TestBed.get(MenuService);
		expect(service).toBeTruthy();
	});
});
