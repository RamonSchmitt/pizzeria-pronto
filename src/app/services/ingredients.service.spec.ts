import { TestBed } from '@angular/core/testing';

import { IngredientsService } from './ingredients.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { IngredientsActions } from '../actions';

describe('IngredientsService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		providers: [
			HttpClient,
			HttpHandler,
			IngredientsActions,
		]
	}));

	it('should be created', () => {
		const service: IngredientsService = TestBed.get(IngredientsService);
		expect(service).toBeTruthy();
	});
});
