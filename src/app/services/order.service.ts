import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '../app.module';
import { Order } from '../models';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class OrderService {
	constructor(
		private ngRedux: NgRedux<AppState>,
		private http: HttpClient) { }

	placeOrder() {
		const order: Order = this.ngRedux.getState().order;
		return this.http.post<Order>('http://localhost:3005/order', order).pipe(
			map(response => response)
		);
	}
}
