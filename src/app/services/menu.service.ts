import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MenuActions } from '../actions';
import { Pizza } from '../models';

@Injectable({
	providedIn: 'root',
})
export class MenuService {
	constructor(
		private http: HttpClient,
		private menuActions: MenuActions) {}

	getMenuList() {
		this.http.get(`http://localhost:3005/pizza`).subscribe((menuList: Pizza[]) => {
			this.menuActions.setMenu(menuList);
		});
	}
}
