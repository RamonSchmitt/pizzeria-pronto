import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { Store, createStore } from 'redux';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { rootReducer } from './reducers';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { DetailsComponent } from './components/details/details.component';
import { OrderComponent } from './components/order/order.component';
import { AddressFormComponent } from './components/order/address-form/address-form.component';
import { FormsModule } from '@angular/forms';
import { IngredientsActions, MenuActions, OrderActions } from './actions';

import { HttpClientModule } from '@angular/common/http';
import { Order, Pizza, Ingredient } from './models';

export interface AppState {
	menu: {
		menuList: Pizza[];
		selectedPizza: Pizza;
	};
	order: Order;
	ingredients: {
		stock: Ingredient[];
	};
}

export const store: Store<AppState> = createStore(rootReducer);

@NgModule({
	declarations: [
		AppComponent,
		MenuListComponent,
		DetailsComponent,
		OrderComponent,
		AddressFormComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		NgReduxModule,
		FormsModule,
	],
	providers: [
		OrderActions,
		MenuActions,
		IngredientsActions,
	],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor(ngRedux: NgRedux<AppState>) {
		ngRedux.provideStore(store);
	}
}
