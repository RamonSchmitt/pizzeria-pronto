import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { DetailsComponent } from './components/details/details.component';
import { OrderComponent } from './components/order/order.component';

const routes: Routes = [
	{ path: '', component: MenuListComponent },
	{ path: 'details', component: DetailsComponent },
	{ path: 'order', component: OrderComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule { }
